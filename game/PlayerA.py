from pygame import sprite, Surface, image, transform
from pygame.rect import Rect
from game import Configure
from game.Bullet import Bullet

ME_SIZE = Configure.ME_SIZE
WIN_WIDTH = Configure.WIN_WIDTH
WIN_HEIGHT = Configure.WIN_HEIGHT


class PlayerA(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)

        self.image = Surface((ME_SIZE, ME_SIZE))
        self.image = image.load("sprites/red.png")
        self.image = transform.scale(self.image, (ME_SIZE, ME_SIZE))
        self.rect = Rect(x, y, ME_SIZE, ME_SIZE)

        self.y0 = self.rect.y + ME_SIZE // 2
        self.x0 = self.rect.x + ME_SIZE // 2

    def update(self, dx, dy):
        if ((self.rect.x + ME_SIZE < WIN_WIDTH - 1 and dx >= 0 or self.rect.x > 1 and dx <= 0) and
                (self.rect.y + ME_SIZE < WIN_HEIGHT - 1 and dy >= 0 or self.rect.y > 1 and dy <= 0)):
            self.rect.x += dx
            self.rect.y += dy
            self.y0 = self.rect.y + ME_SIZE // 2
            self.x0 = self.rect.x + ME_SIZE // 2

    def shoot(self, direction):
        x = self.x0
        y = self.y0
        r = Configure.BULLET_R
        if direction == "U":
            y -= ME_SIZE // 2 + r + 1
        elif direction == "D":
            y += ME_SIZE // 2 + r + 1

        elif direction == "L":
            x -= ME_SIZE // 2 + r + 1
        else:
            x += ME_SIZE // 2 + r + 1

        bullet = Bullet(x, y, direction)
        return bullet

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
