from socket import *

HOST = 'localhost'
PORT = 1234
ADDR = (HOST, PORT)

c = socket(AF_INET, SOCK_STREAM)
c.connect(ADDR)
while True:
    data = input('Введите x, y, k через пробел: ')
    if not data:
        break
    c.send(data.encode())
    data = c.recv(1234)
    if not data:
        break
    print(data.decode('utf8'))
c.close()