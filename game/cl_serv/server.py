import re
from socket import *

HOST = ''
PORT = 1234
ADDR = (HOST, PORT)

s = socket(AF_INET, SOCK_STREAM)
s.bind(ADDR)
s.listen(2)

while True:
    print('Waiting for client...')
    c, addr = s.accept()
    print('Connected:' + str(addr))
    while True:
        data = c.recv(1024)
        if not data:
            print("Соединение разорвано!")
            break
        udata = data.decode('utf8')
        print(udata)
        r = re.findall(r'\S', udata)
        c.send(bytes("x = " + (r[0])+'\n' + "y = " + (r[1]) + '\n' + "k = " + (r[2]),"utf8"))
    c.close()
    s.close()