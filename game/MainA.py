import pygame, time
from game import Configure
from game.PlayerA import PlayerA
from game.PlayerB import PlayerB
from game.Target import Target

DISPLAY = (Configure.WIN_WIDTH, Configure.WIN_HEIGHT)
BACKGROUND_COLOR = Configure.BACKGROUND_COLOR
TITLE = Configure.TITLE


def main():
    move_delay = time.time()
    bullet_create_delay = time.time()


    pygame.init()
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 30)

    screen = pygame.display.set_mode(DISPLAY)
    pygame.display.set_caption(TITLE)
    bg = pygame.Surface(DISPLAY)
    bg.fill(BACKGROUND_COLOR)

    me = PlayerA(300, 300)
    direction = "U"
    target = Target()
    my_bullets = []
    enemy = PlayerB(300, 200)
    other_bullets = []
    other_players = [enemy]

    go = True
    while go:  # Основной цикл программы

        # создать массив игроков, получить эти данные от сервера
        # создать массив пуль игроков, получить эти данные от сервера
        screen.blit(bg, (0, 0))

        if (me.rect.x < 0 or me.rect.y < 0 or target.hp <= 0):
            textsurface = myfont.render('Game over', False, pygame.Color("#ffffff"))
            screen.blit(textsurface, (0, 0))
            go = False

        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                raise SystemExit("QUIT")

        #pygame.event.pump()
        if pygame.key.get_pressed()[pygame.K_ESCAPE]:
            go = False

        dx = 0
        dy = 0
        if time.time() - move_delay > 0.001:
            move_delay = time.time()
            if (pygame.key.get_pressed()[pygame.K_UP]):
                dy = -1
            if (pygame.key.get_pressed()[pygame.K_DOWN]):
                dy = 1
            if (pygame.key.get_pressed()[pygame.K_LEFT]):
                dx = -1
            if (pygame.key.get_pressed()[pygame.K_RIGHT]):
                dx = 1

            if (pygame.key.get_pressed()[pygame.K_1]):
                direction = "U"
            if (pygame.key.get_pressed()[pygame.K_2]):
                direction = "R"
            if (pygame.key.get_pressed()[pygame.K_3]):
                direction = "D"
            if (pygame.key.get_pressed()[pygame.K_4]):
                direction = "L"

        if time.time() - bullet_create_delay > 0.3:
            bullet_create_delay = time.time()
            if (pygame.key.get_pressed()[pygame.K_SPACE]):
                my_bullets.append(me.shoot(direction))



        for elem in my_bullets:
            elem.update(my_bullets)
            elem.draw(screen)

        for elem in other_bullets:
            elem.draw(screen)

        for elem in other_players:
            elem.draw(screen)

        me.update(dx, dy)
        me.draw(screen)
        target.draw(screen, pygame)

        target.сollision_detection(my_bullets, other_players, me)
        pygame.display.update()



if __name__ == "__main__":
    main()